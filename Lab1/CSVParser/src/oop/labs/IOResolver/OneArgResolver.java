package oop.labs.IOResolver;

import java.io.*;

public class OneArgResolver implements ArgsResolver{

    private static final String OUTPUT_FILE = "out.csv";

    @Override
    public  InputStream getInputStream(String[] args) throws IOException{
        String inputFilePath = args[0];
        try{
            return new FileInputStream(inputFilePath);
        }
        catch (IOException exc){
            throw new IOException("Can not open input file: " + inputFilePath);
        }
    }

    @Override
    public OutputStream getOutputStream(String[] args) throws IOException{
        try{
            return new FileOutputStream(OUTPUT_FILE);
        }
        catch (IOException exc){
            throw new IOException("Can not create output file!");
        }
    }
}
