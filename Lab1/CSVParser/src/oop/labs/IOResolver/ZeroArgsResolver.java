package oop.labs.IOResolver;

import java.io.*;

public class ZeroArgsResolver implements ArgsResolver{

    private static final String OUTPUT_FILE = "out.csv";

    @Override
    public InputStream getInputStream(String[] args){
       return System.in;
    }

    @Override
    public OutputStream getOutputStream(String[] args) throws IOException {
        try{
            return new FileOutputStream(OUTPUT_FILE);
        }
        catch (IOException exc){
            throw new IOException("Can not create output file!");
        }
    }
}
