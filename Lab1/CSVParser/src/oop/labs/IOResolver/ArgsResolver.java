package oop.labs.IOResolver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ArgsResolver {

    InputStream getInputStream(String[] args) throws IOException;

    OutputStream getOutputStream(String[] args) throws IOException;

}
