package oop.labs.IOResolver;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public class IOResolver {

    public static class Filler {

        private static final Integer ZERO = 0;
        private static final Integer ONE = 1;
        private static final Integer TWO = 2;
        private static final Integer MAX_NUM_ARGS = 2;

        private static final Map<Integer, ArgsResolver> possibleResolvers = Map.of(
                ZERO, new ZeroArgsResolver(),
                ONE, new OneArgResolver(),
                TWO, new TwoArgsResolver()
        );

        static ArgsResolver getResolver(int numArgs){
            return possibleResolvers.get(numArgs);
        }
    }

    private String[] args;
    private ArgsResolver resolver;

    public IOResolver(String[] args) throws IOException{
        if(args.length > Filler.MAX_NUM_ARGS){
            throw new IOException("Maximum number of arguments: " + Filler.MAX_NUM_ARGS);
        }
        this.args = args;
        resolver = Filler.getResolver(args.length);
    }

    public InputStream getInputStream() throws IOException{
        return resolver.getInputStream(args);
    }

    public OutputStream getOutputStream() throws IOException {
        return resolver.getOutputStream(args);
    }
}
