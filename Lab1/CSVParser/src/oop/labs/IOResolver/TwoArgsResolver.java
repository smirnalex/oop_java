package oop.labs.IOResolver;

import java.io.*;

public class TwoArgsResolver implements ArgsResolver{

    private static final String EXTENSION = ".csv";

    @Override
    public InputStream getInputStream(String[] args) throws IOException{
        String inputFilePath = args[0];
        try{
            return new FileInputStream(inputFilePath);
        }
        catch (IOException exc){
            throw new IOException("Can not open input file: " + inputFilePath);
        }
    }

    @Override
    public OutputStream getOutputStream(String[] args) throws IOException {
        String outputFilePath = args[1];
        if(!CorrectExtension(outputFilePath)){
            throw new IOException("Output file must have \".csv\" extension!");
        }
        try{
            return new FileOutputStream(outputFilePath);
        }
        catch (IOException exc){
            throw new IOException("Can not open output file: " + outputFilePath);
        }
    }

    private boolean CorrectExtension(String path) {
        return path.endsWith(EXTENSION);
    }
}
