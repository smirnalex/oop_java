package oop.labs.parser;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Set;

class WriterCSV {

    private PrintWriter writer;

    WriterCSV(OutputStream oStream) {
        writer = new PrintWriter(oStream, true, StandardCharsets.UTF_8);
    }

    void write(Set<WordStats> data, char delimiter) {
        for(WordStats word: data){
            writer.print(word.getWord());
            writer.print(delimiter);
            writer.print(word.getFrequency());
            writer.print(delimiter);
            writer.println(word.getPercentFreq());
        }
        writer.close();
    }
}
