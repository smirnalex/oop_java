package oop.labs.parser;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class Parser {

    private static final String NON_ALPHANUMERIC = "[\\W]+";
    private static final char COMMA = ',';

    private WordsReader reader;
    private WriterCSV writer;
    private StatsComparator comparator;
    private String delimiter;
    private char delimiterCSV;

    public Parser(InputStream iStream, OutputStream oStream){
        reader = new WordsReader(iStream);
        writer = new WriterCSV(oStream);
        comparator = new StatsComparator();
        delimiter = NON_ALPHANUMERIC;
        delimiterCSV = COMMA;
    }

    public void parse(){
        Set<WordStats> report = reader.read(delimiter, comparator);
        calculatePercentFrequency(report);
        writer.write(report, delimiterCSV);
    }

    private void calculatePercentFrequency(Set<WordStats> report){
        int sumFreq = 0;
        for(WordStats word: report){
            sumFreq += word.getFrequency();
        }
        for(WordStats word: report){
            word.calculatePercentFrequency(sumFreq);
        }
    }
}
