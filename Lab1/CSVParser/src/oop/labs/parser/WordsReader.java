package oop.labs.parser;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

class WordsReader {

    private Scanner scanner;

    WordsReader(InputStream iStream) {
        scanner = new Scanner(iStream, StandardCharsets.UTF_8);
    }

    Set<WordStats> read(String delimiter, Comparator<WordStats> comparator) {
        HashMap<String,WordStats> data = new HashMap<>();
        scanner.useDelimiter(delimiter);
        while (scanner.hasNext()) {
            String word = scanner.next();
            if(data.containsKey(word)){
                data.get(word).increaseFrequency();
            }
            else{
                data.put(word, new WordStats(word));
            }
        }
        TreeSet<WordStats> report = new TreeSet<>(comparator);
        for(Map.Entry<String,WordStats> pair: data.entrySet()){
            report.add(pair.getValue());
        }
        return report;
    }
}
