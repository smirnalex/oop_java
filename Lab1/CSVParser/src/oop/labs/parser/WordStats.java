package oop.labs.parser;

public class WordStats{

    private static final char PERCENT = '%';

    private String word;
    private int frequency;
    private int percentFreq;

    WordStats(String word) {
        this.word = word;
        frequency = 1;
    }

    String getWord(){
        return word;
    }

    void increaseFrequency(){
        frequency++;
    }

    Integer getFrequency() {
        return frequency;
    }

    void calculatePercentFrequency(int sumFreq){
        percentFreq = frequency * 100 / sumFreq;
    }

    String getPercentFreq() {
        return String.valueOf(percentFreq) + PERCENT;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        WordStats wordStats = (WordStats)obj;
        return word.equals(wordStats.getWord());
    }

    @Override
    public int hashCode(){
        return word.hashCode();
    }
}

