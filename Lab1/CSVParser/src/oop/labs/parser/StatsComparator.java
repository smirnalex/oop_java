package oop.labs.parser;

import java.util.Comparator;

public class StatsComparator implements Comparator<WordStats> {

    @Override
    public int compare(WordStats word1, WordStats word2){
        if(word1.equals(word2)){
            return 0;
        }
        if(word1.getFrequency() < word2.getFrequency()){
            return 1;
        }
        else{
            return -1;
        }
    }
}
