package oop.labs;

import oop.labs.IOResolver.IOResolver;
import oop.labs.parser.Parser;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try{
            IOResolver resolver = new IOResolver(args);
            Parser parser = new Parser(resolver.getInputStream(), resolver.getOutputStream());
            parser.parse();
        }
        catch (IOException exc){
            System.out.println(exc.getMessage());
        }
    }
}
