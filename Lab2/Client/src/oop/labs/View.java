package oop.labs;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

class View {

    private Controller controller;

    Stage window;

    TextField clientName;
    TextField host;
    TextField port;
    ToggleGroup groupRadio;
    TextArea data;
    TextField status;

    View(Controller controller){
        this.controller = controller;
    }

    void show(Stage primaryStage){

        window = primaryStage;

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10,10,10,10));
        gridPane.setVgap(10);
        gridPane.setHgap(12);

        Label nameLabel = new Label("Name:");
        GridPane.setConstraints(nameLabel, 0 , 0);

        clientName = new TextField();
        GridPane.setConstraints(clientName, 1 , 0);

        Label hostLabel = new Label("Host:");
        GridPane.setConstraints(hostLabel, 0, 1);

        host = new TextField("localhost");
        GridPane.setConstraints(host, 1, 1);

        Label portLabel = new Label("Port:");
        GridPane.setConstraints(portLabel, 0, 2);

        port = new TextField("8080");
        GridPane.setConstraints(port, 1, 2);

        Label levelLabel = new Label("Level:");
        GridPane.setConstraints(levelLabel, 0 , 3);

        groupRadio = new ToggleGroup();

        RadioButton lowButton = new RadioButton("Low");
        lowButton.setToggleGroup(groupRadio);

        RadioButton normButton = new RadioButton("Normal");
        normButton.setToggleGroup(groupRadio);

        RadioButton highButton = new RadioButton("High");
        highButton.setToggleGroup(groupRadio);

        HBox radioButtons = new HBox(10, lowButton, normButton, highButton);

        GridPane.setConstraints(radioButtons, 1 , 3);

        Label dataLabel = new Label("Data:");
        GridPane.setConstraints(dataLabel, 0 , 4);

        data = new TextArea();
        data.setPrefColumnCount(17);
        data.setPrefRowCount(5);
        GridPane.setConstraints(data, 1 , 4);

        Button sendButton = new Button("Send");
        GridPane.setConstraints(sendButton, 2, 4);
        sendButton.setOnAction(e -> controller.send());

        Button spamButton = new Button("Spam");
        GridPane.setConstraints(spamButton, 2, 3);
        spamButton.setOnAction(e -> controller.spam());

        Button quitButton = new Button("Quit");
        quitButton.setOnAction(e -> controller.quit());
        GridPane.setConstraints(quitButton,2, 0);

        Label statusLabel = new Label("Status:");
        GridPane.setConstraints(statusLabel,0, 5);

        status = new TextField();
        status.setEditable(false);
        GridPane.setConstraints(status,1, 5);

        gridPane.getChildren().addAll(nameLabel, clientName, hostLabel, host, portLabel, port,
                levelLabel, dataLabel, data, radioButtons, sendButton, spamButton, quitButton, status, statusLabel);

        window.setTitle("Client");
        window.setScene(new Scene(gridPane, 410, 300));
        window.show();
    }
}
