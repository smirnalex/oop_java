package oop.labs;

import javafx.scene.control.RadioButton;

import java.io.IOException;

public class Controller {

    private final int SPAM_SIZE = 1000;

    private Model model;
    private View view;

    private boolean firstConnection = true;

    Controller(Model model){
        this.model = model;
    }

    void setView(View view){
        this.view = view;
    }

    void send(){
        try{
            int port = Integer.parseInt(view.port.getText());
            try {
                if (!firstConnection && (port != model.getPort() || !view.host.getText().equals(model.getHost()))) {
                    model.closeSocket(view.clientName.getText());
                    firstConnection = true;
                }
                if (firstConnection) {
                    model.createSocket(view.host.getText(), port);
                    firstConnection = false;
                }
                try {
                    RadioButton selection = (RadioButton) view.groupRadio.getSelectedToggle();
                    String returnMsg = model.writeMessage(view.clientName.getText(),
                            Message.Level.valueOf(selection.getText()), view.data.getText());
                    view.status.setText(returnMsg);
                }
                catch (IOException e){
                    view.status.setText("Failed to send message!");
                    firstConnection = true;
                }
            }
            catch(IOException e){
                view.status.setText("Connection failed!");
                firstConnection = true;
            }
        }
        catch(NumberFormatException e){
            view.status.setText("Port have to have integer format!");
        }
    }

    void quit(){
        try{
            if(!firstConnection){
                model.closeSocket(view.clientName.getText());
            }
        }
        catch(IOException e){
            view.status.setText("Could not close connection!");
        }
        view.window.close();
    }

    void spam(){
        for (int i = 0; i < SPAM_SIZE; i++){
            send();
        }
    }
}
