package oop.labs;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    private static View view;

    public static void main(String[] args) {

        Model model = new Model();
        Controller controller = new Controller(model);
        view = new View(controller);
        controller.setView(view);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        view.show(primaryStage);
    }
}
