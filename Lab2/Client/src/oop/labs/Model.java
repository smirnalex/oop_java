package oop.labs;

import java.io.*;
import java.net.Socket;

class Model {

    private Socket clientSocket;
    private String host;
    private int port;
    private ObjectOutputStream out;
    private DataInputStream in;

    void createSocket(String host, int port) throws IOException{
        clientSocket = new Socket(host, port);
        in = new DataInputStream(clientSocket.getInputStream());
        out = new ObjectOutputStream(clientSocket.getOutputStream());
        this.host = host;
        this.port = port;
    }

    void closeSocket(String clientName) throws  IOException{
        Message msg = new Message(clientName, Message.Level.High, "quit");
        out.writeObject(msg);
        out.flush();
        in.close();
        out.close();
        clientSocket.close();
    }

    String writeMessage(String name, Message.Level importance, String data) throws IOException{
        Message msg = new Message(name, importance, data);
        out.writeObject(msg);
        out.flush();

        return in.readUTF();
    }

    String getHost(){
        return host;
    }

    int getPort(){
        return port;
    }
}
