package oop.labs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

import com.google.devtools.common.options.OptionsParser;

public class Server {

    private static final int MAX_SIZE = 100;
    private static WriterCSV writerCSV;
    private static LinkedList<ServerThread> threadList = new LinkedList<>();
    private static final List<Message> messageSet = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        OptionsParser parser = OptionsParser.newOptionsParser(ServerOptions.class);
        parser.parseAndExitUponError(args);
        ServerOptions options = parser.getOptions(ServerOptions.class);
        if (options.host.isEmpty() || options.port < 0 || options.path.isEmpty()) {
             printUsage(parser);
              return;
        }

        writerCSV = new WriterCSV();
        writerCSV.setFilePath(options.path);

        ServerSocket server = new ServerSocket(options.port);
        var quitThread = new QuitThread(server);
        quitThread.start();

        while (true) {
            try {
                Socket socket = server.accept();
                ServerThread thread = new ServerThread(socket);
                threadList.add(thread);
                thread.start();
            }
            catch(SocketException e){
                for(var thread: threadList){
                    thread.close();
                }
                break;
            }
        }
        writerCSV.write(messageSet);
    }

    static void addMessage(Message msg){
        synchronized (messageSet){
            messageSet.add(msg);
            if (messageSet.size() > MAX_SIZE){
                writerCSV.write(messageSet);
                messageSet.clear();
            }
        }
    }

    private static void printUsage(OptionsParser parser) {
        System.out.println("Usage: java -jar server.jar OPTIONS");
        System.out.println(parser.describeOptions(Collections.emptyMap(),
                OptionsParser.HelpVerbosity.LONG));
    }
}
