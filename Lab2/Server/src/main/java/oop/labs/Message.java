package oop.labs;

import java.io.Serializable;

public class Message  implements Comparable<Message>, Serializable {

    private String sender;
    private Level importance;
    private String information;

    Message(String sender, Level importance, String information){
        this.sender = sender;
        this.importance = importance;
        this.information = information;
    }

    enum Level{
        Low,
        Normal,
        High
    }

    String getInformation() {
        return information;
    }

    Level getImportance() {
        return importance;
    }

    String getSender() {
        return sender;
    }

    @Override
    public int compareTo( Message msg){
        if(msg.importance.ordinal() > importance.ordinal()){
            return 1;
        }
        else if(msg.importance.ordinal() < importance.ordinal()){
            return -1;
        }

        if(msg.sender.equals(sender) && msg.information.equals(information)){
            return 0;
        }
        else{
            return -1;
        }
    }
}

