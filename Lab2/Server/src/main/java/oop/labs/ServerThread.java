package oop.labs;

import java.io.*;
import java.net.Socket;
import java.lang.Thread;

public class ServerThread extends Thread {

    private Socket socket;

    ServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (var in = new ObjectInputStream(socket.getInputStream());
            var out = new DataOutputStream(socket.getOutputStream())) {

            Message msg;
            try {
                while (true) {
                    msg = (Message) in.readObject();
                    if (msg.getInformation().equals("quit")) {
                        break;
                    }
                    out.writeUTF("Message has read successfully!");
                    out.flush();
                    Server.addMessage(msg);
                }
            }
            catch (IOException e) {
                out.writeUTF("Failed to read message!");
            }
            catch (ClassNotFoundException e) {
                out.writeUTF("Could not convert message to class Message!");
            }
        }
        catch (IOException e) {
            System.out.println("Socket streams have crashed!");
        }
        close();
    }

    void close(){
        try{
            socket.close();
        }
        catch(IOException e){
            System.out.println("Could not close socket!");
        }

    }
}
