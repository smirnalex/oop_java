package oop.labs;

import java.io.IOException;
import java.util.Scanner;
import java.net.ServerSocket;

public class QuitThread extends Thread {

    private ServerSocket server;

    QuitThread(ServerSocket server) {
        this.server = server;
    }

    @Override
    public void run() {
        var scanner = new Scanner(System.in);
        while (true) {
            if (scanner.nextLine().equals("quit")) {
                try {
                    server.close();
                } catch (IOException e) {
                    System.out.println("Could not close Server!");
                }
                break;
            }
        }
    }
}