package oop.labs;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;

class WriterCSV {

    private String filePath;

    void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    void write(List<Message> messageSet){
        try(FileWriter writer = new FileWriter(filePath, true)){
            char delimiter = ',';
            for(var msg : messageSet){
                writer.append(msg.getImportance().name());
                writer.append(delimiter);
                writer.append(msg.getSender());
                writer.append(delimiter);
                writer.append(msg.getInformation() + '\n');
            }
            writer.flush();
        }
        catch(IOException e){
            System.out.println("Could not open output file!");
        }
    }
}
