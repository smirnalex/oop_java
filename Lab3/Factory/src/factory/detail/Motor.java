package factory.detail;

public class Motor extends Detail {

    private static int orderNumber = 1;

    public Motor(){
        id = orderNumber;
        orderNumber++;
    }
}
