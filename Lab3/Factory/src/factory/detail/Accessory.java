package factory.detail;

public class Accessory extends Detail {

    private static int orderNumber = 1;

    public Accessory(){
        id = orderNumber;
        orderNumber++;
    }
}
