package factory;

import factory.detail.Detail;

import java.util.ArrayDeque;
import java.util.Deque;

public class Storage<T extends Detail> {

    private final Deque<T> details = new ArrayDeque<>();
    private int capacity;

    public Storage(int capacity){
        this.capacity = capacity;
    }

    public int getSize(){
        synchronized(details){
            return details.size();
        }
    }

    public synchronized void storeDetail(T detail){
        //synchronized (details) {
            while (details.size() >= capacity) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    break;
                }
//                if (details.size() >= capacity) {
//                    notify();
//                }
            }
            details.add(detail);
            notify();
        //}
    }

    public synchronized T getDetail(){
        //synchronized (details) {
            while (details.size() <= 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    break;
                }
//                if (details.size() <= 0) {
//                    notify();
//                }
            }

            T detail = details.remove();
            notify();
            return detail;
        //}
    }
}
