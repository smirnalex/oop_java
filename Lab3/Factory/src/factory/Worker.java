package factory;

import main.Controller;
import factory.detail.Accessory;
import factory.detail.Body;
import factory.detail.Motor;

public class Worker {

    private static int carsProduced = 0;

    private static Controller controller;

    private static Storage<Accessory> accessoryStorage;
    private static Storage<Motor> motorStorage;
    private static Storage<Body> bodyStorage;
    private static Storage<Car> carStorage;


    public static void setStorages(Storage<Accessory> accessoryStorage, Storage<Motor> motorStorage,
                  Storage<Body> bodyStorage, Storage<Car> carStorage){
        Worker.accessoryStorage = accessoryStorage;
        Worker.motorStorage = motorStorage;
        Worker.bodyStorage = bodyStorage;
        Worker.carStorage = carStorage;
    }

    public static void setController(Controller controller){
        Worker.controller = controller;
    }

    public void buildCar(){
        Accessory accessory = accessoryStorage.getDetail();
        Motor motor = motorStorage.getDetail();
        Body body = bodyStorage.getDetail();
        Car car = new Car(body, motor, accessory);
        carStorage.storeDetail(car);
        carsProduced++;

        controller.updateAccessories(Integer.toString(accessoryStorage.getSize()));
        controller.updateMotors(Integer.toString(motorStorage.getSize()));
        controller.updateBodies(Integer.toString(bodyStorage.getSize()));
        controller.updateCars(Integer.toString(carStorage.getSize()));
        controller.updateCarsProduced(Integer.toString(carsProduced));
    }
}
