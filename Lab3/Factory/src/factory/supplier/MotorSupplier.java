package factory.supplier;

import factory.Storage;
import factory.detail.Motor;

public class MotorSupplier extends Supplier {

    private static Storage<Motor> storage;

    public static void setStorage(Storage<Motor> storage){
        MotorSupplier.storage = storage;
    }

    private static int detailsProduced = 0;

    @Override
    public void produceDetail(){
        storage.storeDetail(new Motor());
        detailsProduced++;
        controller.updateMotorsProduced(Integer.toString(detailsProduced));
    }
}