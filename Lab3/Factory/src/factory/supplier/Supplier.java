package factory.supplier;

import main.Controller;

public abstract class Supplier {

    protected static Controller controller;

    public static void setController(Controller controller){
        Supplier.controller = controller;
    }

    abstract public void produceDetail();
}




