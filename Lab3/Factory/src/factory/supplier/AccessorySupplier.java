package factory.supplier;

import factory.Storage;
import factory.detail.Accessory;

public class AccessorySupplier extends Supplier {

    private static Storage<Accessory> storage;

    public static void setStorage(Storage<Accessory> storage){
        AccessorySupplier.storage = storage;
    }

    private static int detailsProduced = 0;

    @Override
    public void produceDetail(){
        storage.storeDetail(new Accessory());
        detailsProduced++;
        controller.updateAccessoriesProduced(Integer.toString(detailsProduced));
    }
}