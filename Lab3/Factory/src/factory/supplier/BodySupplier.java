package factory.supplier;

import factory.Storage;
import factory.detail.Body;

public class BodySupplier extends Supplier {

    private static Storage<Body> storage;

    public static void setStorage(Storage<Body> storage){
        BodySupplier.storage = storage;
    }

    private static int detailsProduced = 0;

    @Override
    public void produceDetail(){
        storage.storeDetail(new Body());
        detailsProduced++;
        controller.updateBodiesProduced(Integer.toString(detailsProduced));
    }
}
