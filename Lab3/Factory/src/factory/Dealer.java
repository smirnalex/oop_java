package factory;

import main.Controller;
import main.SaleLogger;

public class Dealer {

    private SaleLogger logger = new SaleLogger();

    private static int carsSold = 0;
    private static int orderNumber = 1;

    private int id;

    private static Storage<Car> carStorage;
    private static Controller controller;

    public Dealer(){
        id = orderNumber;
        orderNumber++;
    }

    public static void setCarStorage(Storage<Car> carStorage){
        Dealer.carStorage = carStorage;
    }
    public static void setController(Controller controller){
        Dealer.controller = controller;
    }

    public void sellCar(){
        Car car = carStorage.getDetail();
        carsSold++;
        logger.logSale(id, car);
        controller.updateSold(Integer.toString(carsSold));
    }
}
