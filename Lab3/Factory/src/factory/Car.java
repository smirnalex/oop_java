package factory;

import factory.detail.Accessory;
import factory.detail.Body;
import factory.detail.Detail;
import factory.detail.Motor;

public class Car extends Detail {

    private static int orderNumber = 1;

    private Body body;
    private Motor motor;
    private Accessory accessory;

    public Car(Body body, Motor motor, Accessory accessory){
        id = orderNumber;
        orderNumber++;
        this.body = body;
        this.motor = motor;
        this.accessory = accessory;
    }

    public int getAccessoryID() {
        return accessory.getId();
    }

    public int getMotorID() {
        return motor.getId();
    }

    public int getBodyID() {
        return body.getId();
    }
}
