package main;

import factory.Car;
import factory.Dealer;
import factory.Storage;
import factory.Worker;
import factory.detail.Accessory;
import factory.detail.Body;
import factory.detail.Motor;
import factory.supplier.AccessorySupplier;
import factory.supplier.BodySupplier;
import factory.supplier.MotorSupplier;
import factory.supplier.Supplier;
import threadpool.ThreadPool;

public class FactoryEmulator {

    private Controller controller;

    private ThreadPool accessorySuppliersPool;
    private ThreadPool motorSuppliersPool;
    private ThreadPool bodySuppliersPool;
    private ThreadPool workersPool;
    private ThreadPool dealersPool;

    private Storage<Accessory> accessoryStorage;
    private Storage<Motor> motorStorage;
    private Storage<Body> bodyStorage;
    private Storage<Car> carStorage;

    private boolean isStarted = false;


    public FactoryEmulator(Controller controller){
        this.controller = controller;
        Supplier.setController(controller);
        Worker.setController(controller);
        Dealer.setController(controller);

        initStorages();

        initAccessorySuppliers();
        initMotorSupplier();
        initBodySupplier();

        initWorkers();

        initDealers();

        if(controller.logSale){
            SaleLogger.configureLogger(controller.nameLogFile);
        }
    }

    void updateAccessorySuppliersSpeed(int speed){
        if (isStarted){
            accessorySuppliersPool.updateSpeed(speed);
        }
        else{
            accessorySuppliersPool.setSpeed(speed);
        }

    }

    void updateMotorSupplierSpeed(int speed){
        if (isStarted){
            motorSuppliersPool.updateSpeed(speed);
        }
        else{
            motorSuppliersPool.setSpeed(speed);
        }
    }

    void updateBodySupplierSpeed(int speed){
        if (isStarted){
            bodySuppliersPool.updateSpeed(speed);
        }
        else{
            bodySuppliersPool.setSpeed(speed);
        }
    }

    void updateDealersSpeed(int speed){
        if (isStarted){
            dealersPool.updateSpeed(speed);
        }
        else{
            dealersPool.setSpeed(speed);
        }
    }

    void start(){
        isStarted = true;
        motorSuppliersPool.start();
        bodySuppliersPool.start();
        accessorySuppliersPool.start();
        dealersPool.start();
        workersPool.start();
    }

    void end(){
        motorSuppliersPool.shutDown();
        bodySuppliersPool.shutDown();
        accessorySuppliersPool.shutDown();
        dealersPool.shutDown();
        workersPool.shutDown();
    }

    private void initStorages(){
        accessoryStorage = new Storage<>(Integer.parseInt(controller.accessoryStorageSize.getText()));
        motorStorage = new Storage<>(Integer.parseInt(controller.motorStorageSize.getText()));
        bodyStorage = new Storage<>(Integer.parseInt(controller.bodyStorageSize.getText()));
        carStorage = new Storage<>(Integer.parseInt(controller.carStorageSize.getText()));
    }

    private void initAccessorySuppliers(){
        int numberAccessorySuppliers = Integer.parseInt(controller.accessorySuppliers.getText());
        AccessorySupplier[] accessorySuppliers = new AccessorySupplier[numberAccessorySuppliers];
        for(int i = 0; i < numberAccessorySuppliers; i++){
            accessorySuppliers[i] = new AccessorySupplier();
        }
        AccessorySupplier.setStorage(accessoryStorage);
        Runnable[] accessorySuppliersTasks = new Runnable[numberAccessorySuppliers];
        for(int i = 0; i < numberAccessorySuppliers; i++){
            accessorySuppliersTasks[i] = accessorySuppliers[i]::produceDetail;
        }
        accessorySuppliersPool = new ThreadPool(accessorySuppliersTasks);
    }

    private void initMotorSupplier(){
        MotorSupplier motorSupplier = new MotorSupplier();
        MotorSupplier.setStorage(motorStorage);
        Runnable motorSupplierTask = motorSupplier::produceDetail;
        motorSuppliersPool = new ThreadPool( new Runnable[]{motorSupplierTask});
    }

    private void initBodySupplier(){
        BodySupplier bodySupplier = new BodySupplier();
        BodySupplier.setStorage(bodyStorage);
        Runnable bodySupplierTask = bodySupplier::produceDetail;
        bodySuppliersPool = new ThreadPool(new Runnable[]{bodySupplierTask});
    }

    private void initWorkers(){
        int numberWorkers = Integer.parseInt(controller.workers.getText());
        Worker[] workers = new Worker[numberWorkers];
        for (int i = 0; i < numberWorkers; i++){
            workers[i] = new Worker();
        }
        Worker.setStorages(accessoryStorage, motorStorage, bodyStorage, carStorage);
        Runnable[] workersTasks = new Runnable[numberWorkers];
        for(int i = 0; i < numberWorkers; i++){
            workersTasks[i] = workers[i]::buildCar;
        }
        workersPool = new ThreadPool(workersTasks);
    }

    private void initDealers(){
        int numberDealers = Integer.parseInt(controller.dealers.getText());
        Dealer[] dealers = new Dealer[numberDealers];
        for(int i = 0; i < numberDealers; i++){
            dealers[i] = new Dealer();
        }
        Dealer.setCarStorage(carStorage);
        Runnable[] dealersTasks = new Runnable[numberDealers];
        for(int i = 0; i < numberDealers; i++){
            dealersTasks[i] = dealers[i]::sellCar;
        }
        dealersPool = new ThreadPool(dealersTasks);
    }
}
