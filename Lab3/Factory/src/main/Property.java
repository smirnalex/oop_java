package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Property {

    private static final String PATH_TO_PROPERTIES = "src/resources/config.properties";

    public static Map<String,String> getProperties() throws IOException{

        try (FileInputStream fileInputStream = new FileInputStream(PATH_TO_PROPERTIES)){

            Map<String,String> propMap = new HashMap<>();

            Properties prop = new Properties();
            prop.load(fileInputStream);

            propMap.put("BodyStorageSize", prop.getProperty("BodyStorageSize"));
            propMap.put("MotorStorageSize", prop.getProperty("MotorStorageSize"));
            propMap.put("AccessoryStorageSize", prop.getProperty("AccessoryStorageSize"));
            propMap.put("CarStorageSize", prop.getProperty("CarStorageSize"));
            propMap.put("AccessorySuppliers", prop.getProperty("AccessorySuppliers"));
            propMap.put("Workers", prop.getProperty("Workers"));
            propMap.put("Dealers", prop.getProperty("Dealers"));
            propMap.put("LogSale", prop.getProperty("LogSale"));
            propMap.put("NameLogFile", prop.getProperty("NameLogFile"));
            return propMap;
        }
        catch (IOException e){
            throw new IOException("Failed to open file: " + PATH_TO_PROPERTIES);
        }
    }

}