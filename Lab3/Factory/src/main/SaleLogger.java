package main;

import factory.Car;

import java.io.IOException;
import java.util.logging.*;

public class SaleLogger {

    private static final Logger LOGGER = Logger.getLogger(SaleLogger.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
        LOGGER.setUseParentHandlers(false);
    }

    public static void configureLogger(String nameLogFile){
        try{
            FileHandler handler = new FileHandler(nameLogFile);
            handler.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(handler);
            LOGGER.setLevel(Level.INFO);
        }
        catch (IOException e){
            System.err.println("Failed to open Log File");
        }
    }

    public synchronized void logSale(int dealerID, Car car){
        LOGGER.log(Level.INFO, "Dealer {0}: Auto {1} (Body: {2}, Motor: {3}, Accessory: {4})\n",
                new Object[]{dealerID, car.getId(), car.getBodyID(), car.getMotorID(), car.getAccessoryID()});
    }
}
