package main;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Map;

public class Controller {

    @FXML
    Label accessorySuppliers;
    @FXML
    Label motorStorageSize;
    @FXML
    Label bodyStorageSize;
    @FXML
    Label accessoryStorageSize;
    @FXML
    Label carStorageSize;
    @FXML
    Label workers;
    @FXML
    Label dealers;

    @FXML
    private TextField motorsProduced;
    @FXML
    private TextField bodiesProduced;
    @FXML
    private TextField accessoriesProduced;
    @FXML
    private TextField carsProduced;
    @FXML
    private TextField motors;
    @FXML
    private TextField bodies;
    @FXML
    private TextField accessories;
    @FXML
    private TextField cars;
    @FXML
    private TextField sold;

    @FXML
    private Slider motorSpeed;
    @FXML
    private Slider bodySpeed;
    @FXML
    private Slider accessorySpeed;
    @FXML
    private Slider dealerSpeed;

    @FXML
    private Button workButton;

    Boolean logSale;
    String nameLogFile;

    private boolean exitFlag = false;

    private FactoryEmulator factoryEmulator;

    @FXML
    private void initialize(){
        try {
            Map<String,String> properties = Property.getProperties();
            setProperties(properties);
        }
        catch (IOException e){
            System.err.println(e.getMessage());
        }

        factoryEmulator = new FactoryEmulator(this);

        motorSpeed.valueProperty().addListener((observable, oldValue, newValue) ->
            factoryEmulator.updateMotorSupplierSpeed(newValue.intValue())
        );
        bodySpeed.valueProperty().addListener((observable, oldValue, newValue) ->
            factoryEmulator.updateBodySupplierSpeed(newValue.intValue())
        );
        accessorySpeed.valueProperty().addListener((observable, oldValue, newValue) ->
            factoryEmulator.updateAccessorySuppliersSpeed(newValue.intValue())
        );
        dealerSpeed.valueProperty().addListener((observable, oldValue, newValue) ->
            factoryEmulator.updateDealersSpeed(newValue.intValue())
        );
    }

    @FXML
    private void work() {
        if(exitFlag){
            factoryEmulator.end();
            Stage stage = (Stage)workButton.getScene().getWindow();
            stage.close();
        }
        else{
            factoryEmulator.start();
            workButton.setText("END");
            exitFlag = true;
        }
    }

    private void setProperties(Map<String,String> properties){
        accessorySuppliers.setText(properties.get("AccessorySuppliers"));
        motorStorageSize.setText(properties.get("MotorStorageSize"));
        bodyStorageSize.setText(properties.get("BodyStorageSize"));
        accessoryStorageSize.setText(properties.get("AccessoryStorageSize"));
        carStorageSize.setText(properties.get("CarStorageSize"));
        workers.setText(properties.get("Workers"));
        dealers.setText(properties.get("Dealers"));
        logSale = Boolean.valueOf(properties.get("LogSale"));
        nameLogFile = properties.get(("NameLogFile"));
    }

    @FXML
    public void updateMotorsProduced(String value){
            Platform.runLater(() -> motorsProduced.setText(value));
    }
    @FXML
    public void updateBodiesProduced(String value){
            Platform.runLater(() -> bodiesProduced.setText(value));
    }
    @FXML
    public void updateAccessoriesProduced(String value){
            Platform.runLater(() -> accessoriesProduced.setText(value));
    }
    @FXML
    public void updateCarsProduced(String value){
            Platform.runLater(() -> carsProduced.setText(value));
    }
    @FXML
    public void updateMotors(String value){
            Platform.runLater(() -> motors.setText(value));
    }
    @FXML
    public void updateBodies(String value){
            Platform.runLater(() -> bodies.setText(value));
    }
    @FXML
    public void updateAccessories(String value){
            Platform.runLater(() -> accessories.setText(value));
    }
    @FXML
    public void updateCars(String value){
            Platform.runLater(() -> cars.setText(value));
    }
    @FXML
    public void updateSold(String value){
            Platform.runLater(() -> sold.setText(value));
    }
}
