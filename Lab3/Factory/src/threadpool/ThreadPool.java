package threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ThreadPool {

    private ScheduledExecutorService[] executors;
    private List<ScheduledFuture<?>> currentTasks;
    private Runnable[] tasks;
    private int speed = 1;

    public ThreadPool(Runnable[] tasks){
        executors = new ScheduledExecutorService[tasks.length];
        currentTasks = new ArrayList<>();
        for(int i = 0; i < tasks.length; i++){
            executors[i] = Executors.newScheduledThreadPool(1);
        }
        this.tasks = tasks;
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }

    public void updateSpeed(int speed){
        this.speed = speed;
        for (int i = 0; i < tasks.length; i++){
            currentTasks.get(i).cancel(false);
        }
        for(int i = 0; i < tasks.length; i++){
            if(speed == 0){
                currentTasks.set(i, executors[i].scheduleWithFixedDelay(tasks[i], 0, 1, TimeUnit.MINUTES));
            }
            else{
                currentTasks.set(i, executors[i].scheduleWithFixedDelay(tasks[i], 0, 1000/speed, TimeUnit.MILLISECONDS));
            }
        }
    }

    public void start(){
        for(int i = 0; i < tasks.length; i++){
            currentTasks.add(executors[i].scheduleWithFixedDelay(tasks[i], 0, 1000/speed, TimeUnit.MILLISECONDS));
        }
    }

    public void shutDown(){
        for(int i = 0; i < tasks.length; i++) {
            executors[i].shutdownNow();
        }
    }
}
